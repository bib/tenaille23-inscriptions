TEMPLATE_2 = """
_______________________________________________________________________________________________
    /!\ IL Y A EÛT UN PROBLÈME AVEC LE PROGRAMME QUI GÈRE LES INSCRIPTIONS /!\
 Merci de toujours prendre en compte les dernières informations qui vous ont étés transmises !
 C'est donc ce mail qu'il faut croire (jusqu'au prochain s'il y en a un !)
 On est désolé.e.s pour les nombreux mails et la confusion.
 La Tenaille 
_______________________________________________________________________________________________

Salut,

Vous avez été très nombreux.ses à vous inscrire aux ateliers, merci pour 
l'enthousiasme que vous manifestez. Au vu du grand nombre d'inscription 
nous avons choisis de déterminer la liste des participant.es par tirage au sort.

Nous t'avons inscrit à deux ateliers :

Atelier 1 : {}

Atelier 2 : {}

Les informations plus détaillées concernant l'atelier te seront communiquées
directement par l'intervenant.e. On lui transmet ton contact.

En cas de désistement, préviens nous rapidement car il y a plus d'une centaine
de personnes sans atelier, par manque de place.

Les ateliers sont à prix libre (prix conseillé 10 euros par atelier pour couvrir
les frais de l'évènement) mais n'hésite pas à adapter selon tes moyens. 
Pense à prévoir de la monnaie.

Aussi, les ateliers des dimanche 13 et samedi 19 octobres sont sans pré-inscription
ainsi que les soirées du week-end et de la semaine! 
La programmation des soirées :

 - Dimanche 13 : Scène ouverte sans pression ni talent au BIB hackerspace (80 impasse Flouch, Montpellier) à partir de 19h00.
 - Lundi 14    : Conférence "Auto défense féministe en mécanique auto: comment bien choisir sa voiture d'occasion" au Quartier Généreux (2 quai des tanneurs, Montpellier) à partir de 19h00 
 - Mardi 15    : Apéro // Rencontre // TADAMs* 20h00 - 22h00 à la Bricole (80 rue du Faubourg Figuerolles, Montpellier)
 - Mercredi 16 : Apéro // Rencontre // TADAMs* 20h00 - 22h00 au BIB Hackerspace (80 impasse Flouch, Montpellier)
 - Vendredi 18 : Apéro // Rencontre // TADAMs* 18h00 - 22h00 au Bouzarts (3 avenue Saint-Lazare, Montpellier)
 - Samedi 19   : Assemblée générale de clôture, deux spectacle -Morsure & Tsef zon(e)-, une performance de Krump, un concert de rap par Tahra, une performance live par LW2, des dj-sets par Cabale, XDOLL femDJ...

* Tadams = Temps d'Ateliers et de Discussions Autogérés (et Mortels...). Pour en proposer c'est par là -> https://semestriel.framapad.org/p/tadam-de-la-tenaille-aa7s?lang=fr

Pour toutes les infos sur les horaires, les performances et l'accessibilité des lieux, n'hésite pas à regarder sur 
notre site internet : https://latenaille.org/edition2024/programme.html

Pour rappel, le festival ne propose pas d'hébergement.

A très vite

Tenaillement,
"""

TEMPLATE_1 = """
_______________________________________________________________________________________________
    /!\ IL Y A EÛT UN PROBLÈME AVEC LE PROGRAMME QUI GÈRE LES INSCRIPTIONS /!\
 Merci de toujours prendre en compte les dernières informations qui vous ont étés transmises !
 C'est donc ce mail qu'il faut croire (jusqu'au prochain s'il y en a un !)
 On est désolé.e.s pour les nombreux mails et la confusion.
 La Tenaille 
_______________________________________________________________________________________________

Salut,

Vous avez été très nombreux.ses à vous inscrire aux ateliers, merci pour 
l'enthousiasme que vous manifestez. Au vu du grand nombre d'inscription 
nous avons choisis de déterminer la liste des participant.es par tirage au sort.

Nous n'avons pu t'inscrire qu'à un seul atelier :

Atelier : {}

Tu es sur liste d'attente pour tes autres choix d'atelier. 
Nous te contacterons si des places se libèrent.

Les informations plus détaillées concernant l'atelier te seront communiquées 
directement par l'intervenant.e. On lui transmet ton contact.

En cas de désistement, préviens nous rapidement car il y a plus d'une centaine
de personnes sans atelier, par manque de place.

Aussi, les ateliers des dimanche 13 et samedi 19 octobres sont sans pré-inscription
ainsi que les soirées du week-end et de la semaine! 
La programmation des soirées :

 - Dimanche 13 : Scène ouverte sans pression ni talent au BIB hackerspace (80 impasse Flouch, Montpellier) à partir de 19h00.
 - Lundi 14    : Conférence "Auto défense féministe en mécanique auto: comment bien choisir sa voiture d'occasion" au Quartier Généreux (2 quai des tanneurs, Montpellier) à partir de 19h00 
 - Mardi 15    : Apéro // Rencontre // TADAMs* 20h00 - 22h00 à la Bricole (80 rue du Faubourg Figuerolles, Montpellier)
 - Mercredi 16 : Apéro // Rencontre // TADAMs* 20h00 - 22h00 au BIB Hackerspace (80 impasse Flouch, Montpellier)
 - Vendredi 18 : Apéro // Rencontre // TADAMs* 18h00 - 22h00 au Bouzarts (3 avenue Saint-Lazare, Montpellier)
 - Samedi 19   : Assemblée générale de clôture, deux spectacle -Morsure & Tsef zon(e)-, une performance de Krump, un concert de rap par Tahra, une performance live par LW2, des dj-sets par Cabale, XDOLL femDJ...

* Tadams = Temps d'Ateliers et de Discussions Autogérés (et Mortels...). Pour en proposer c'est par là -> https://semestriel.framapad.org/p/tadam-de-la-tenaille-aa7s?lang=fr

Pour toutes les infos sur les horaires, les performances et l'accessibilité des lieux, n'hésite pas à regarder sur 
notre site internet : https://latenaille.org/edition2024/programme.html

Pour rappel, le festival ne propose pas d'hébergement.

A très vite

Tenaillement,
"""

TEMPLATE_0 = """
_______________________________________________________________________________________________
    /!\ IL Y A EÛT UN PROBLÈME AVEC LE PROGRAMME QUI GÈRE LES INSCRIPTIONS /!\
 Merci de toujours prendre en compte les dernières informations qui vous ont étés transmises !
 C'est donc ce mail qu'il faut croire (jusqu'au prochain s'il y en a un !)
 On est désolé.e.s pour les nombreux mails et la confusion.
 La Tenaille 
_______________________________________________________________________________________________

Salut,

Vous avez été très nombreux.ses à vous inscrire aux ateliers, merci pour 
l'enthousiasme que vous manifestez. Au vu du grand nombre d'inscription nous
avons choisis de déterminer la liste des participant.es par tirage au sort.

Malheureusement, pour le moment tu es sur liste d'attente pour tes choix d'atelier.

Nous espérons que de la place se libère et que tu profites d'au moins un atelier 
pendant le festival. Si c'est le cas, nous te recontacterons. 

Cependant, les ateliers des dimanche 13 et samedi 19 octobres sont sans pré-inscription
ainsi que les soirées du week-end et de la semaine!
Il y a aussi les TADAMs*, n'hésite pas à y venir !

La programmation des soirées :

 - Dimanche 13 : Scène ouverte sans pression ni talent au BIB hackerspace (80 impasse Flouch, Montpellier) à partir de 19h00.
 - Lundi 14    : Conférence "Auto défense féministe en mécanique auto: comment bien choisir sa voiture d'occasion" au Quartier Généreux (2 quai des tanneurs, Montpellier) à partir de 19h00 
 - Mardi 15    : Apéro // Rencontre // TADAMs* 20h00 - 22h00 à la Bricole (80 rue du Faubourg Figuerolles, Montpellier)
 - Mercredi 16 : Apéro // Rencontre // TADAMs* 20h00 - 22h00 au BIB Hackerspace (80 impasse Flouch, Montpellier)
 - Vendredi 18 : Apéro // Rencontre // TADAMs* 18h00 - 22h00 au Bouzarts (3 avenue Saint-Lazare, Montpellier)
 - Samedi 19   : Assemblée générale de clôture, deux spectacle -Morsure & Tsef zon(e)-, une performance de Krump, un concert de rap par Tahra, une performance live par LW2, des dj-sets par Cabale, XDOLL femDJ...

* Tadams = Temps d'Ateliers et de Discussions Autogérés (et Mortels...). Pour en proposer c'est par là -> https://semestriel.framapad.org/p/tadam-de-la-tenaille-aa7s?lang=fr

Pour toutes les infos sur les horaires, les performances et l'accessibilité des lieux, n'hésite pas à regarder sur 
notre site internet : https://latenaille.org/edition2024/programme.html

Pour rappel, le festival ne propose pas d'hébergement.

A très vite

Tenaillement,
"""

TEMPLATE_CANCEL = """
_______________________________________________________________________________________________
    /!\ IL Y A EÛT UN PROBLÈME AVEC LE PROGRAMME QUI GÈRE LES INSCRIPTIONS /!\
 Merci de toujours prendre en compte les dernières informations qui vous ont étés transmises !
 C'est donc ce mail qu'il faut croire (jusqu'au prochain s'il y en a un !)
 On est désolé.e.s pour les nombreux mails et la confusion.
 La Tenaille 
_______________________________________________________________________________________________

Salut,

Nous avons bien reçu ta demande de désistement pour cet atelier :

 - {}

Merci de nous avoir tenu.e.s au courant !

A très vite

Tenaillement,
"""

TEMPLATE_SUMMARY = """
Salut,

Nous avons réalisé une phase de traitement des inscriptions pour La Tenaille.

Tu trouveras en pièce jointe la version la plus à jour de la liste d'inscrit.e.s pour
ton atelier :

 - {}

Ce document sera réenvoyé lors des futurs traitements pour y intégrer les désistements
et les nouvelles inscriptions (au cas ou il reste des places libres).

Il contient la liste des personnes, avec leur mail et téléphone, pour que tu puisses
les transmettres des informations sur l'atelier. Ca peut être une bonne idée d'envoyer
un mail de rappel la veille de l'atelier.

A très vite

Tenaillement,
"""