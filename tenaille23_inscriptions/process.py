"""Module de traitement.

Ce module contient la fonction principale de traitement.
"""
# Importation de modules Python
from collections import defaultdict
from functools import partial
from random import Random

# Importation des modules de notre programme
from .data import Request
from .data import UNPROCESSED, ACCEPTED, QUEUED, REJECTED, CANCELLED
from .rules import check_rejection, is_available_for, has_same_workshop
from .rules import count_accepted, count_available
from .stats import Stats


def process(workshops, state, persons, submissions, logfile=None, seed=None):
    """TODO
    
    """
    if logfile is False:
        log = lambda *a: None
    else:
        log = partial(print, file=logfile)
    rand = Random(seed)
    
    # 1. on met à jour l'état avec les nouveaux résultats du formulaire
    # -----------------------------------------------------------------

    # on créé une liste temporaire pour accumuler les demandes par personne
    requests = defaultdict(list)

    # pour chaque entrée du formulaire
    for submission in submissions.values():
        # pour chaque choix dans cette entrée
        for choice in submission.choices:
            # on ajoute le choix à la liste de demandes temporaire
            requests[submission.person_email].append((submission.id, choice))

    # NOTE: j'ai encore quelques doutes sur la manière de gérer ça.
    # l'idée c'est de gérer correctement le cas ou une personne refait des
    # demandes après avoir été rejetée lors du premier traitement, sans que
    # ses nouvelles demandes deviennent des choix 6/7/8/9/10.

    # NOTE: là, je crois que l'idée dans cette partie du code est ok, mais que le gérer
    # ici ça fait que plus loin, dans la boucle de répartition, on met pas à jour la valeur
    # d'ordre quand les demandes d'une personne sont rejetées. et ça c'est pas ouf.

    # FIXME: trouver une meilleur manière de traiter l'ordre de préférence dans la boucle
    # de répartition. probablement qu'il faut le recalculer à chaque itération, en fait.

    # en pratique, la plupart (>90%) des rejets sont causés par la règle "pas plus de 2 ateliers"
    # du coup c'est pas si grave, les personnes dont l'ordre de préférence est pas respecté
    # c'est surtout des personnes qui ont déjà deux ateliers, pour elleux ça change rien du tout

    # c'est un truc à voir pour d'autres versions du programme

    # pour chaque personne dans cette liste de demandes temporaire
    for person_email, person_requests in requests.items():
        # on initialise un compteur d'ordre de préférence, à 1
        order = 1
        # pour chaque choix de cette personne
        for submission_id, workshop_id in person_requests:
            # si ce choix n'est pas encore dans la liste de traitement
            if (workshop_id, person_email) not in state:
                # on créé une nouvelle demande, avec comme état "Non traitée"
                request = Request(workshop_id, submission_id, person_email, order, UNPROCESSED, False, "")
                # on enregistre cette demande dans notre tableau d'état
                state[workshop_id, person_email] = request
            # si la demande n'est ni annulée, ni rejetée
            if state[workshop_id, person_email].state not in (REJECTED, CANCELLED):
                # on incrémente le compteur d'ordre de préférence
                order += 1

    # 2. on rassemble les demandes non traitées
    # -----------------------------------------

    # un créé une liste temporaire
    unprocessed = list()

    # pour chaque demande dans la liste de demandes
    for request in state.values():
        # si cette demande est non traitée, ou en attente
        if request.state in (UNPROCESSED, QUEUED):
            # on l'ajoute à la liste temporaire
            unprocessed.append(request)

    # 3. on trie les demandes non traitées
    # ------------------------------------

    # En tenant compte de :
    #  - du nombre d'ateliers acceptés pour cette personne,
    #    pour prioriser les personnes avec le moins d'atelier
    #  - de l'ordre de préférence de cette demande
    #  - d'une valeur aléatoire déterministe

    unprocessed.sort(key=lambda req: (count_accepted(persons[req.person_email], state), req.order, rand.random()))

    # 4. on répartit les demandes
    # ---------------------------

    # pour chaque demande non traitée
    for request in unprocessed:

        # 4.1. d'abord, un récupère les objets correspondants
        # ---------------------------------------------------

        # on récupère l'objet Workshop correspondant
        workshop = workshops[request.workshop_id]
        # on récupère l'objet Submission correspondant
        submission = submissions[request.submission_id]
        # on récupère l'objet Person correspondant
        person = persons[request.person_email]

        # 4.2. ensuite, on compte les valeurs utilisés pour le traitement
        # ---------------------------------------------------------------

        # on compte le nombre de demandes acceptées pour cette personne
        accepted = count_accepted(person, state)
        # on compte le nombre de places disponibles dans cet atelier
        available = count_available(workshop, state)

        log(f"On traite la demande de {person.email} pour '{workshop.title}'")
        log(f"\t C'est une demande d'ordre {request.order}, cette personne a {accepted} ateliers acceptés.")

        # 4.3. ensuite, on traite les raisons possibles de rejet
        # ------------------------------------------------------

        if check_rejection(request, accepted, state, workshops):
            log(f"\t Cette demande est REJETEE : {request.rejection}.")
            request.state = REJECTED
            continue

        # 4.4. ensuite, on attribute cette demande à un atelier
        # -----------------------------------------------------

        log(f"\t Cet atelier a {available} places disponibles.")

        # si il reste une place disponible
        if available > 0:
            # on accepte la demande
            log("\t Cette demande est ACCEPTEE !")
            request.state = ACCEPTED
        else:
            # sinon, on met en attente
            log("\t Cette demande est EN ATTENTE")
            request.state = QUEUED

    # 5. on vérifie si certaines des requêtes en attente peuvent être rejetées
    # ------------------------------------------------------------------------

    for request in state.values():
        if request.state == QUEUED:
            person = persons[request.person_email]
            workshop = workshops[request.workshop_id]
            accepted = count_accepted(person, state)
            log(f"On traite la demande en attente de {person.email} pour '{workshop.title}'")
            log(f"\t C'est une demande d'ordre {request.order}, cette personne a {accepted} ateliers acceptés.")
            if check_rejection(request, accepted, state, workshops):
                log(f"\t Cette demande est REJETEE : {request.rejection}.")
                request.state = REJECTED
                continue
            else:
                log("\t Cette demande est maintenue EN ATTENTE")

    # on retourne des statistiques de ce traitement
    return Stats(workshops, persons, submissions, state, unprocessed)
