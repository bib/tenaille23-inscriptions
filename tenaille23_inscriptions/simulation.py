"""Module de simulation

Ce module contient une fonction qui applique notre algorithme plein de fois
pour déterminer le tirage au sort le plus "optimal".

"""
# Importation de modules Python
from random import randint
from copy import deepcopy

# Importation des modules de notre programme
from .process import process

def simulate(workshops, state, submissions, persons, iters=1000):
    """Cette fonction applique notre processus de traitement un certain
    nombre de fois, avec une graine différente à chaque fois, et retourne
    la graine la plus optimale par rapport au nombre de personnes avec 0 ateliers.
    
    """
    print("Simulation bayésienne itérative")
    print("-------------------------------")
    print()
    print("Aucune graine n'a été fournie au script, nous allons lancer")
    print(f"l'algorithme {iters} fois de manière à donner des ateliers")
    print("au plus grand nombre de personnes possible")
    print()
    print("Plus précisèment, on optimise en premier pour minimiser le ")
    print("nombre de personnes avec 0 ateliers, et en second pour maximiser")
    print("le nombre d'ateliers acceptés.")
    print("")

    # on initialise une liste de résultats (statistiques et graine associée)
    results = list()
    # puis, pour chaque essai voulu
    for i in range(iters):
        print("\rSimulation...", end=" ")
        # on génère une nouvelle graine
        seed = randint(0, 2**32)
        # on applique le traitement, sans envoyer de mail, sur une copie
        # temporaire de notre tableau d'état du traitement, et sans écrire
        # de journal
        stats = process(workshops, deepcopy(state), persons, submissions, seed=seed, logfile=False)
        # on ajoute les statistiques résultantes à notre tableau, avec la graine
        results.append((stats, seed))
        # on indique là ou on est sur le terminal
        print(f"({i}/{iters})", end="", flush=True)
    print("\rSimulation terminée.", flush=True)
    print()

    # on trie notre liste de résultats, en mettant les résultats avec le moins de personnes
    # sans ateliers et avec le moins de places disponibles tout en haut
    results.sort(key=lambda result: (result[0].persons_with_0, result[0].total_available))
    # on sélectionne le premier résultat (le "meilleur", du coup)
    stats, seed = results[0]

    # on affiche des infos sur le résultat en question
    print("Le meilleur résultat est : ")
    print(f" - Nombre de personnes avec 0 ateliers: {stats.persons_with_0}")
    print(f" - Places disponibles : {stats.total_available}")
    print(f" - Graine: {hex(seed)}")
    print()

    # on retourne la graine qu'on a obtenu par notre simulation
    return seed
