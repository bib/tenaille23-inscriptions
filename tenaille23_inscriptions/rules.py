"""Module de règles de traitement

Ce module contient des fonctions qui analysent l'état actuel pour pouvoir
appliquer les règles de traitement décidées collectivemment.
"""
# Importation des modules de notre programme
from .data import ACCEPTED, CANCELLED, REJECTED

def is_overlapping(workshop1, workshop2):
    """Est-ce que deux atelier sont en-même temps ?
    
    """
    # FIXME: peut-être faudrait mettre une marge ?

    # si l'atelier 2 commence après l'atelier 1, et que l'atelier 2 finit avant l'atelier 1
    if workshop2.start > workshop1.start and workshop2.end <= workshop1.end:
        return True # oui
    # si l'atelier 1 commence après l'atelier 2, et que l'atelier 1 finit avant l'atelier 2
    if workshop1.start > workshop2.start and workshop1.end <= workshop2.end:
        return True # oui
    return False    # non

def is_available_for(request, state, workshops):
    """Est-ce que une personne a déjà un atelier en même temps ?
    
    """
    # pour chaque demande
    for other in state.values():
        # si cette demande concerne la même personne et a été acceptée
        if other.person_email == request.person_email and other.state == ACCEPTED:
            # si cette demande est en meme temps
            if is_overlapping(workshops[other.workshop_id], workshops[request.workshop_id]):
                return False  # ça va pas être possible
    return True

def has_same_workshop(request, state, workshops):
    """Est-ce que une personne est déjà inscrit.e à une autre session du même atelier ?
    
    Pour appliquer cette règle, on utilise le fait que plusieurs sessions du même atelier
    seront représentées comme plusieurs ateliers avec le même titre.
    """
    # pour chaque demande
    for other in state.values():
        # si cette demande concerne la même personne et a été acceptée
        if other.person_email == request.person_email and other.state == ACCEPTED:
            # si cette demande est une autre session du meme atelier
            if workshops[other.workshop_id].title == workshops[request.workshop_id].title:
                return True  # ça va pas être possible
    return False

def count_accepted(person, state):
    """A combien d'ateliers cette personne a été inscrit.e ?
    
    """
    # on initialise une valeur de retour
    result = 0
    # pour chaque demande
    for request in state.values():
        # si cette demande concerne la personne en question et a été acceptée
        if request.person_email == person.email and request.state == ACCEPTED:
            # on incrémente la valeur de retour
            result += 1
    # on retourne le résultat
    return result

def count_available(workshop, state):
    """Combien de places reste-t-il dans cet atelier ?
    
    """
    # on initialise une valeur de retour à la capacité de l'atelier
    result = workshop.capacity
    # pour chaque demande
    for request in state.values():
        # si cette demande concerne l'atelier en question et a été acceptée
        if request.workshop_id == workshop.id and request.state == ACCEPTED:
            # on décrémente le nombre de place restantes
            result -= 1
    # on retourne le résultat
    return max(0, result)


def check_rejection(request, accepted, state, workshops):
    """Est-ce que cette requête doit être rejetée ?"""
    if accepted >= 2:
        # plus de 2 ateliers, on enregistre le message de rejet
        request.rejection = "déjà inscrit.e à deux ateliers."
    elif not is_available_for(request, state, workshops):
        # pas disponible, on enregistre le message de rejet
        request.rejection = "déjà inscrit.e à un atelier en même temps."
    elif has_same_workshop(request, state, workshops):
        # autre session, on enregistre le message de rejet
        request.rejection = "déjà inscrit.e à une autre session de cet atelier."
    return bool(request.rejection)