"""Module de notifications par mail

Ce module contient le code utilisé pour envoyer des notifications
aux personnes qui ont demandé à s'inscrire, pour les tenir au courant
de l'état de leurs demandes.

"""
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.charset import Charset, QP
from email.utils import make_msgid, formatdate
from email import encoders
from time import sleep

from tenaille23_inscriptions.templates import TEMPLATE_SUMMARY

UTF8_AS_QP = Charset('utf-8')
UTF8_AS_QP.body_encoding = QP

def send_notification(server, sender, recipient, subject, text):
    """Envoyer une notif par mail.
    
    """
    print(f"Envoi de la notification à {recipient}, de {sender} : " )
    print(" - ", subject)
    sleep(1)
    message = MIMEMultipart("alternative")
    message["Subject"] = subject
    message["From"] = sender
    message["To"] = recipient
    message["Date"] = formatdate()
    message["Message-Id"] = make_msgid(domain="latenaille.org")
    part1 = MIMEText(text, "plain", _charset=UTF8_AS_QP)
    message.attach(part1)
    server.sendmail(
        sender, recipient, message.as_string()
    )

def send_summary(server, sender, recipient, subject, name, summary):
    print(f"Envoi de la synthèse à {recipient}, de {sender} : " )
    print(" - ", subject)
    sleep(1)
    message = MIMEMultipart()
    message["Subject"] = subject
    message["From"] = sender
    message["To"] = recipient
    message["Date"] = formatdate()
    message["Message-Id"] = make_msgid(domain="latenaille.org")
    part1 = MIMEText(TEMPLATE_SUMMARY.format(name), _charset=UTF8_AS_QP)
    with open(summary, encoding='utf-8') as file:
        part2 = MIMEText(file.read(), "csv")
        part2.add_header('Content-Disposition',
                        'attachment; filename=inscriptions.csv')
    message.attach(part1)
    message.attach(part2)
    server.sendmail(
        sender, recipient, message.as_string()
    )
