"""Module d'entrée/sortie

Ce module contient les définitions de structures représentant les différents
"objets" avec lesquels notre programme va interagir :
 - Workshop (ateliers)
 - Person (personne)
 - Request (demande d'inscription)
 - Submission (entrée du formulaire)

 Ce module contient aussi des fonctions d'entrée/sortie, qui permettent de lire
 et écrire depuis les fichiers CSV qui contiennent les données du traitement :
  - read_workshops : lire les définitions d'ateliers
  - read_submissions : lire les entrées du formulaire
  - read_state : lire le tableau d'état du traitement
  - write_state : écrire le tableau d'état du traitement

"""

# Importations de modules Python
# ============================================================================

import csv                           # module de lecture/écriture CSV
from dataclasses import dataclass    # classes de données
from datetime import datetime        # représentation du temps
from typing import List              # liste de données

# Valeurs de représentation de l'état du traitement
# ============================================================================

UNPROCESSED = "Non traité"
ACCEPTED = "Accepté"
REJECTED = "Refusé"
QUEUED = "Attente"
CANCELLED = "Annulé"

# Classes de données
# ============================================================================

@dataclass
class Workshop:
    """Définition d'atelier, d'après le formulaire d'inscription.
    
    On a besoin notamment :
     - de l'identifiant de l'atelier, pour faire correspondre les
       résultats du formulaire
     - du titre de l'atelier, pour les messages d'information et pour
       le tableau final
     - de l'horaire de début et de fin de l'atelier, pour déterminer si
       deux ateliers sont en même temps
     - de l'emplacement de l'atelier, pour l'afficher dans le tableau final
     - de la jauge de l'atelier, pour l'algorithme de répartition
     
     """
    id: int
    title: str
    start: datetime
    end: datetime
    location: int
    capacity: int
    email: str

@dataclass
class Person:
    """Définition de personnes inscrit.e à un atelier.
    
    On a besoin de ces infos pour différencier les personnes et avoir les infos
    nécessaires pour leur transmettre les mails de notification.
    
    """
    name: str
    email: str
    phone: str

@dataclass
class Request:
    """Définition de demandes à un atelier.
    
    C'est notre objet de traitement principal. On y stocke :
     - à quel atelier la demande correspond
     - à quelle entrée du formulaire ça correspond
     - à quelle personne ça correspond
     - l'ordre de préférence de la demande en question
     - l'état de traitement de la demande en question
       (non traitée / acceptée / rejetée / annulée)
     - est-ce qu'on a envoyé un mail pour notifier de l'état de cette demande
     - la raison éventuelle du rejet

     """
    workshop_id: int
    submission_id: int
    person_email: str
    order: int
    state: str
    notified: bool
    rejection: str

@dataclass
class Submission:
    """Définition d'entrée du formulaire.
    
    On l'utilise en début du traitement, quand on lit les résultats du formulaire,
    sa structure correspond à ce qu'on trouve dans le tableau exporté par FramaForms.
    
    """
    id: int
    person_email: str
    choices: List[int]

# Fonctions de lecture CSV
# ============================================================================

def read_workshops(path):
    """Lire les définitions d'ateliers depuis le chemin donné.
    
    Retourne un tableau (dictionnaire) d'ateliers, indexé par l'identifiant
    de cet atelier.

    """
    # on initialise notre valeur de retour
    result = dict()
    # on ouvre le fichier
    with open(path, encoding="utf8") as file:
        # on créé un lecteur CSV
        reader = csv.reader(file)
        # on saute la ligne d'en-tête
        next(reader, None)
        # ensuite, pour chaque ligne
        for row in reader:
            # on lit les valeurs de cette ligne
            workshop_id = int(row[0])
            workshop_title = str(row[1])
            # on décode l'horaire pour avoir l'heure de début et de fin
            start_hour, end_hour = row[3].split("-")
            # on combine le jour et l'horaire début/fin, puis on décode ça
            # pour créer un objet 'datetime' qu'on pourra utiliser pour 
            # faire des comparaisons temporelles plus simplement
            start_raw = ' '.join((row[2], start_hour))
            end_raw = ' '.join((row[2], end_hour))
            workshop_start = datetime.strptime(start_raw, "%A %d %B %Y %Hh%M")
            workshop_end = datetime.strptime(end_raw, "%A %d %B %Y %Hh%M")
            workshop_location = row[4]
            workshop_capacity = int(row[5])
            email = row[6]
            # on créé un objet Workshop à partir des valeurs décodées
            workshop = Workshop(workshop_id, workshop_title, workshop_start,
                                workshop_end, workshop_location, workshop_capacity, email)
            # on enregistre cette valeur dans le tableau
            result[workshop_id] = workshop
    # on retourne le tableau qu'on a rempli
    return result

def read_submissions(path):
    """Lire les résultats du formulaire depuis le chemin donné.
    
    Retourne un tableau (dictionnaire) de résultats, indexé par l'identifiant
    de l'entrée dans le formulaire, ainsi qu'un tableau de personnes demandeur.euses,
    indexé par l'adresse email de cette personne.

    """
    # on initialise nos valeurs de retour
    submissions = {}
    users = {}
    # on ouvre le fichier
    with open(path, encoding="utf8") as file:
        # on créé un lecteur CSV
        reader = csv.reader(file)
        # on saute les 3 lignes d'en tête
        for _ in range(3):
            next(reader)
        # puis, pour chaque ligne
        for row in reader:
            # on lit les valeurs de cette ligne
            submission_id = int(row[0])
            choices = [int(row[col]) for col in (9, 10, 11, 12) if row[col]]
            person_name = str(row[13])
            person_email = str(row[14])
            person_phone = str(row[15])
            # on enregistre l'entrée dans le tableau de retour
            submissions[submission_id] = Submission(submission_id, person_email, choices)
            # on enregistre la personne dans le tableau de retour
            users[person_email] = Person(person_name, person_email, person_phone)
    # on retourne les tableaux remplis
    return submissions, users

def read_state(path):
    """Lire le tableau d'état du traitement depuis le chemin donné.

    Retourne un tableau (dictionnaire) de demandes, indexé par l'identifiant de l'atelier
    combiné à l'adresse email de la personne demandeur.euse.
    
    """
    # on initialise la valeur de retour
    state = dict()
    # on ouvre le fichier
    with open(path, encoding="utf8") as file:
        # on créé un lecteur CSV
        reader = csv.reader(file)
        # on saute la ligne d'en-tête
        next(reader, None)
        # puis, pour chaque ligne
        for index, row in enumerate(reader, 2):
            # on lit les valeurs de cette ligne
            workshop_id = int(row[0])
            submission_id = int(row[7])
            person_email = str(row[9])
            choice_order = int(row[11])
            notified = bool(row[13])
            status = str(row[12])
            rejection = str(row[14])
            # on vérifie que la colonne "Etat" a une valeur cohérente
            if status not in (UNPROCESSED, ACCEPTED, REJECTED, QUEUED, CANCELLED):
                print(f"ERREUR: Valeur incompréhensible pour la colonne Etat ({status}) "
                      f"à la ligne {index}.")
                print("Impossible de continuer, arrêt du programme.")
                exit(1)
            # on créé l'objet Request correspondant
            request = Request(workshop_id, submission_id, person_email,
                              choice_order, status, notified, rejection)
            # on enregistre cette demande dans le tableau de retour
            state[workshop_id, person_email] = request
    # on retourne le tableau qu'on a rempli
    return state

# Fonctions d'écriture CSV
# ============================================================================

def write_state(path, state, workshops, persons):
    """Ecrire le tableau d'état du traitement.
    
    """
    # on ouvre un fichier (en mode d'écriture)
    with open(path, 'w', encoding="utf8") as file:
        # on créé un enregistreur CSV
        writer = csv.writer(file)
        # on écrit la ligne d'en-tête
        writer.writerow(("Atelier (id)", "Atelier", "Jour", "Début", "Fin", 
                         "Emplacement", "Capacité", "Soumission", "Nom", 
                         "Email", "Téléphone", "Choix", "Etat", 
                         "Notifié.e", "Raison du rejet"))
        # ensuite, pour chaque demande
        for request in state.values():
            # on prépare les valeurs à écrire
            workshop = workshops[request.workshop_id]
            person = persons[request.person_email]
            notified = "X" if request.notified else ""
            # on formatte les horaires de début/fin d'une manière lisible
            day = datetime.strftime(workshop.start, "%A %d")
            start = datetime.strftime(workshop.start, "%Hh%M")
            end = datetime.strftime(workshop.end, "%Hh%M")
            # on écrit la ligne de valeurs
            writer.writerow((workshop.id, workshop.title, day, start, end, 
                             workshop.location, workshop.capacity, request.submission_id, 
                             person.name, person.email, person.phone, request.order, 
                             request.state, notified, request.rejection))
            
def format_workshop(workshop):
    """Générer un texte décrivant l'atelier, pour les mails.
    
    """
    return "{} (lieu: {}) (horaire: {}, de {} à {})".format(
        workshop.title,
        workshop.location,
            datetime.strftime(workshop.start, "%A %d"),
            datetime.strftime(workshop.start, "%Hh%M"),
            datetime.strftime(workshop.end, "%Hh%M"),
    )

def write_form_list(path, state, workshops):
    """Ecrire la liste d'ateliers à saisir dans le formulaire, pour y 
    indiquer les ateliers qui sont complets.
    
    """
    ... # TODO

def write_by_workshop_tables(path, state, workshops):
    """Ecrire les tableaux par atelier, à transmettre aux personnes
    intervenant.e.s de cet atelier.
    
    """
    ... # TODO

def write_by_person_tables(path, state, persons):
    """Ecrire les tableaux par personnes, utiles pour regarder qui
    n'a pas eu d'atelier du tout.
    
    """
    ... # TODO
