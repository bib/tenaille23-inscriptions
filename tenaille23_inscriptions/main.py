"""Gestion des inscriptions pour La Tenaille 2024

Ce programme lit les résultats d'un formulaire (ainsi qu'une description des ateliers)
pour faire une répartition équitable et optimale des places dans les ateliers, en utilisant
plusieurs règles :

 - traitement des choix par ordre de préférence
 - pas plus de 2 ateliers par personnes
 - pas d'inscriptions à des ateliers simultanés pour la même personne
 - pas d'inscriptions à deux sessions du même atelier pour la même personne

Par défaut, une simulation itérative est réalisée pour essayer d'identifier la répartition
la plus optimale possible, de manière à minimiser le nombre de personnes sans atelier. C'est
un problème NP-difficile, qu'on peut exprimer comme un problème de satisfiabilité booléenne (SAT),
et dans l'idéal on aurait utilisé un solveur adéquat, mais pas trop le temps pour ça, donc on applique
la solution brute : réaliser plein de tirage au sort et prendre le plus optimal.

Il est aussi possible d'exécuter le programme avec une graine de départ (qui détermine le tirage
au sort), en utilisant l'argument "--seed".

Il reste quelques petits problèmes, notamment par rapport au traitement des ordres de préférence et
de leur relation aux demandes "rejetées" ou "annulées" : les ordres de préférence restants devraient
être décalés, actuellement ce n'est pas le cas.

Après son exécution, le programme (sans --dry-run) écrit la nouvelle version du tableau des demandes
dans "state.csv", et exporte des tableaux utiles (par-personne, par-atelier, ainsi qu'un tableau pour
mettre à jour les champs du formulaire) dans le dossier "output/".

Quand le programme est exécuté avec --notify, des mails sont envoyés aux personnes pour leur indiquer
l'état du traitement de leur demande, puis le tableau d'état est mis à jour pour indiquer que ces mails
ont été envoyés.

Ce programme s'attend à l'existence de 3 fichiers en entrée :
 - workshops.csv (description des ateliers)
 - state.csv (état du traitement, fichier vide au départ)
 - submissions.csv (résultats du formulaire)

Quand ce programme est utilisé avec FramaForms, le fichier submissions.csv doit être
exporté par FramaForms avec les règles suivantes :
 - format CSV
 - séparateur: virgule (',')
 - clefs brutes
 - le reste: par défaut

Pour désister des demandes, il faut éditer le fichier "state.csv" et :
 - dans la ligne correspondante à la demande à désister
 - passer la colonne "Etat" à "Annulé" (avec l'accent et la majuscule)
 - passer la colonne "Notifié.e" à "" (retirer le X qui devrait y être présent)


"""

# Importation de modules Python
import csv
from datetime import datetime
from locale import LC_TIME, setlocale
from argparse import ArgumentParser, RawDescriptionHelpFormatter, ArgumentDefaultsHelpFormatter
from os import mkdir
from os.path import join as path_join, exists
import smtplib
import ssl

from tenaille23_inscriptions.rules import count_available

# Importation des modules de notre programme
from .data import CANCELLED, read_workshops, read_submissions, read_state
from .data import write_state, format_workshop, UNPROCESSED, ACCEPTED
from .mail import send_notification, send_summary
from .process import process
from .simulation import simulate
from .templates import TEMPLATE_0, TEMPLATE_1, TEMPLATE_2, TEMPLATE_CANCEL

WORKSHOPS_PATH = "./workshops.csv"
SUBMISSIONS_PATH = "./submissions.csv"
STATE_PATH = "./state.csv"
EXPORT_PATH = "./output/"

# point d'entrée principal
def main():
    """Point d'entrée principal.

    """

    # on créé un parser d'arguments
    class ArgFormatter(ArgumentDefaultsHelpFormatter, RawDescriptionHelpFormatter):  pass
    parser = ArgumentParser(
        prog='tenaille23-inscriptions',
        formatter_class=ArgFormatter,
        description=__doc__,
        epilog="ACAB, nonobstant.\n",
    )

    # on renseigne les arguments de notre programme
    parser.add_argument('-d', '--dry-run', action="store_true",
        help="Execution de test (n'écrit aucun fichier, envoie les mails à l'adresse de test)")
    parser.add_argument('-n', '--notify', action="store_true",
        help="Envoyer les mails de notification qui n'ont pas encore été envoyés")
    parser.add_argument('--seed', metavar='X',
        help="Graine de départ (si non fournie, on fait une simulation avec N itérations).")
    parser.add_argument('--iters', metavar='N', default=1312, type=int,
        help="Nombre d'itérations pour la simulation du traitement")
    parser.add_argument('--workshops', metavar='ATELIERS', default="./workshops.csv",
        help="Fichier de description des ateliers")
    parser.add_argument('--state', metavar='ETAT', default="./state.csv",
        help="Fichier d'état du traitement")
    parser.add_argument('--submissions', metavar='FORM', default="./submissions.csv",
        help="Fichier de résultats du formulaire")
    parser.add_argument('--output', metavar='SORTIE', default="./output/",
        help="Dossier d'exportation des résultats")

    # on interprète les arguments
    args = parser.parse_args()
    workshops_path = args.workshops
    state_path = args.state
    submissions_path = args.submissions
    output_path = args.output
    seed = int(args.seed, 16) if args.seed else None
    dry_run = args.dry_run
    notify = args.notify
    iters = args.iters
    log_path = path_join(output_path, 'log.txt')

    # on définit une localisation francophone, nécessaire pour l'interprétation
    # des représentations d'heure et de date
    setlocale(LC_TIME, "fr_FR.UTF-8")

    print("Programme de répartition des ateliers - La Tenaille 2024")
    print("========================================================")
    print()
    print("\tVersion: 0.13.12")
    print("\tDate: 14 septembre 2023")
    print()
    print("Lecture des fichiers de données")
    print("-------------------------------")
    print()
    print("Fichiers de données :")
    print(f" - Ateliers: {workshops_path}")
    print(f" - Résultats: {submissions_path}")
    print(f" - Etat du traitement: {state_path}")
    print()

    try:
        if not exists(output_path):
            print("On créé le dossier de sortie...", end=" ")
            mkdir(output_path)
            print("terminé.")
        print("On lit les définitions d'ateliers...", end=" ")
        workshops = read_workshops(workshops_path)
        print(f"terminé ({len(workshops)} ateliers).")
        print("On lit les résultats du formulaire...", end=" ")
        submissions, persons = read_submissions(submissions_path)
        print(f"terminé ({len(submissions)} résultats pour {len(persons)} personnes).")
        print("On lit l'état actuel du traitement'...", end=" ")
        state = read_state(state_path)
        processed = sum(1 for r in state.values() if r.state != UNPROCESSED)
        print(f"terminé ({processed} demandes déjà traitées).")
    except FileNotFoundError as ex:
        print()
        print(f"Erreur: {ex}")
        exit(1)

    print()
    print("Lecture des sources de données terminée.")
    print()

    if seed is None:
        seed = simulate(workshops, state, submissions, persons, iters)

    if True:
        print("Tirage au sort final")
        print("--------------------")
        print()
        print(f"On va maintenant utiliser la graine choisie ({hex(seed)}) pour")
        print("refaire le tirage au sort de la manière sélectionnée comme optimale.")
        print()
        print("Options:")
        print(" - Graine: ", hex(seed))
        print(" - Fichier de journal: ", log_path)
        print(" - Envoi des mails: ", "OUI" if notify else "NON")
        print()
        with open(log_path, 'w', encoding='utf-8') as logfile:
            print("Traitement... ", end="")
            stats = process(workshops, state, persons, submissions, seed=seed, logfile=logfile)
            print("terminé.")
        print()

        print("Statistiques finales")
        print("--------------------")
        print()
        stats.show()
        print()

    if notify:
        print("Envoi des mails de notification")
        print("-------------------------------")
        print()
        print("Connexion au serveur de mails...")
        context = ssl.create_default_context()
        host = input("Serveur : ")
        sender = input("Adresse : ")
        password = input("Mot de passe : ")
        if dry_run:
            test_recipient = input("Adresse pour le test des mails : ")
        with smtplib.SMTP_SSL(host, 465, context=context) as server:
            server.login(sender, password)
            # ateliers acceptés
            for person in persons.values():
                recipient = test_recipient if dry_run else person.email
                # on recupere les demandes associées a cette personne
                requests = [request for request in state.values() if request.person_email == person.email]
                # si ces demandes ont toutes été notifiées, on passe a la suivante
                if all(request.notified for request in requests):  continue

                # On extrait toute les demandes de la personne qui sont acceptées
                acceptedRequests = [request for request in requests if request.state == ACCEPTED]
                # On compte le nombre de requête acceptée 
                accepted = len(acceptedRequests)

                # on génère le texte du mail
                subject, text = None, None
                if accepted == 2:
                    subject = "La Tenaille 2024 - Résultats inscription (2 ateliers acceptés)"
                    text = TEMPLATE_2.format(
                        format_workshop(workshops[acceptedRequests[0].workshop_id]),
                        format_workshop(workshops[acceptedRequests[1].workshop_id]),
                    )
                elif accepted == 1:
                    subject = "La Tenaille 2024 - Résultats inscription (1 ateliers accepté)"
                    text = TEMPLATE_1.format(
                        format_workshop(workshops[acceptedRequests[0].workshop_id]),
                    )
                elif accepted == 0: #Si y'a aucune demande acceptée du coup
                    subject = "La Tenaille 2024 - Résultats inscription (en attente)"
                    text = TEMPLATE_0
                else:
                    assert False, "wtf"
                # Si on est en dryrun, on ajoute le mail de la personne au sujet pour pouvoir l'identifier
                if dry_run : subject = subject + person.email
                send_notification(server, sender, recipient, subject, text)
                for request in requests:
                    request.notified = True
            # désistements
            for person in persons.values():
                recipient = test_recipient if dry_run else person.email
                # on créé une liste pour accumuler les demandes de cette personne
                requests = []
                # pour chaque demande
                for request in state.values():
                    # si elle concerne la personne en question
                    if request.person_email == person.email:
                        if request.state == CANCELLED and not request.notified:
                            # on l'ajoute à notre liste temporaire
                            requests.append(request)
                # pour chaque demande
                for request in requests:
                    subject = "La Tenaille 2024 - Désistement d'un atelier"
                    
                    # Si on est en dryrun, on ajoute le mail de la personne au sujet pour pouvoir l'identifier
                    if dry_run: subject = suject + " (" + person.email + ")"
                    
                    text = TEMPLATE_CANCEL.format(
                        format_workshop(workshops[request.workshop_id]),
                    )
                    send_notification(server, sender, recipient, subject, text)
                    request.notified = True
        print()

    if not dry_run:
        print("Enregistrement des données")
        print("--------------------------")
        print()

        print("Ecriture du tableau d'état... ", end="")
        write_state(state_path, state, workshops, persons)
        print("terminé.")

        print("Exportation des données")
        print("-----------------------")
        print()

        with open(path_join(EXPORT_PATH, 'formulaire.txt'), 'w', encoding="utf-8") as file:
            for workshop_id, workshop in workshops.items():
                full = count_available(workshop, state) == 0
                extra = "[COMPLET] " if full else ""
                file.write(f"{workshop_id}|{extra}{format_workshop(workshop)}\n")

        print("Nouvelle version du formulaie exportée...")

        for workshop_id, workshop in workshops.items():
            workshop_table = path_join(EXPORT_PATH, workshop.title.replace('/', '-') + '.csv')
            with open(workshop_table, 'w', encoding='utf-8') as file:
                writer = csv.writer(file)
                writer.writerow(("Atelier", "Jour", "Début", "Fin", "Nom", "Email", "Téléphone", "Statut"))
                for request in state.values():
                    person = persons[request.person_email]
                    if request.workshop_id == workshop_id:
                        if request.state in (ACCEPTED, CANCELLED):
                            day =  datetime.strftime(workshop.start, "%A %d")
                            start = datetime.strftime(workshop.start, "%Hh%M")
                            end = datetime.strftime(workshop.end, "%Hh%M")
                            writer.writerow((workshop.title, day, start, end, person.name, person.email, person.phone, request.state))

        print("Synthèses par ateliers exportées...")

    if notify:
        print()
        print("Envoi des mails aux intervenant.e.s")
        print("-----------------------------------")
        print()

        with smtplib.SMTP_SSL(host, 465, context=context) as server:
            # TODO: envoyer ça par mail à l'intervenante
            server.login(sender, password)
            for workshop_id, workshop in workshops.items():
                workshop_table = path_join(EXPORT_PATH, workshop.title.replace('/', '-') + '.csv')
                for recipient in workshop.email.split():
                    if dry_run:
                        recipient = test_recipient
                    subject = "La Tenaille 2024 - Inscriptions"
                    send_summary(server, sender, recipient, subject, format_workshop(workshop), workshop_table)
                    print(f"synthèse pour {workshop.title} envoyée à {recipient}")

    print()
    print("Execution terminée, bonne journée !")

if __name__ == '__main__':
    main()
