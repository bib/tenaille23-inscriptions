"""Module de statistiques

Ce module contient une classe (mal écrite) qui calcule et affiche des
statistiques par rapport au traitement réalisé.
"""
# Importation de modules Python
from functools import partial

# Importation des modules de notre programme
from .data import ACCEPTED, REJECTED, QUEUED, CANCELLED
from .rules import count_available, count_accepted

# pylint: disable=too-few-public-methods,too-many-instance-attributes
class Stats:
    """Classes de calcul et stockage de statistiques du traitement."""
    def __init__(self, workshops, persons, submissions, state, unprocessed):
        """Calculer des statistiques à partir des objets données. """
        # FIXME: ce code est vraiment pas très efficace, il faudrait le reécrire
        #        comme une itération sur 'state' qui incrémente des compteurs,
        #        là on lit 'state' plein de fois pas nécessaires.
        self.processed_count = len(unprocessed)
        self.workshops_count = len(workshops)
        self.workshop_places = [(count_available(workshop, state), workshop)
                                for workshop in workshops.values()]
        self.workshop_places.sort(key=lambda x: x[0], reverse=True)
        self.persons_accepted = [(count_accepted(person, state), person)
                                 for person in persons.values()]
        self.persons_accepted.sort(key=lambda x: x[0], reverse=True)
        self.total_places = sum(workshop.capacity for workshop in workshops.values())
        self.total_available = sum(x for x, _ in self.workshop_places)
        self.total_submissions = len(submissions)
        self.total_persons = len(persons)
        self.total_requests = len(state)
        self.total_accepted = sum(1 for r in state.values() if r.state == ACCEPTED)
        self.total_rejected = sum(1 for r in state.values() if r.state == REJECTED)
        self.total_queued = sum(1 for r in state.values() if r.state == QUEUED)
        self.total_cancelled = sum(1 for r in state.values() if r.state == CANCELLED)
        self.persons_with_2 = sum(1 for x, _ in self.persons_accepted if x == 2)
        self.persons_with_1 = sum(1 for x, _ in self.persons_accepted if x == 1)
        self.persons_with_0 = sum(1 for x, _ in self.persons_accepted if x == 0)
        self.workshops_full = sum(1 for x, _ in self.workshop_places if x == 0)

    def show(self, file=None):
        """Afficher les statistiques calculées. """
        # on définit une fonction 'log' qui écrit dans le fichier demandé
        log = partial(print, file=file)
        # on affiche tous les trucs et lalala
        log(f" - {self.workshops_count} ateliers")
        log(f" - {self.workshops_full} ateliers complets")
        log(f" - {self.total_places} places en atelier")
        log(f" - {self.total_available} places restantes")
        log(f" - {self.total_submissions} saisies du formulaire")
        log(f" - {self.total_persons} personnes distinctes")
        log(f" - {self.total_requests} demandes")
        log(f" - {self.processed_count} demandes traitées dans cette phase")
        log(f" - {self.total_accepted} demandes acceptées")
        log(f" - {self.total_rejected} demandes rejetées")
        log(f" - {self.total_queued} demandes en attente")
        log(f" - {self.total_cancelled} demandes annulées")
        log(f" - {self.persons_with_2} personnes inscrit.e.s à 2 ateliers")
        log(f" - {self.persons_with_1} personnes inscrit.e.s à 1 ateliers")
        log(f" - {self.persons_with_0} personnes inscrit.e.s à 0 ateliers")
