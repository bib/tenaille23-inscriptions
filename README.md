# Tenaille 2023 - Programme de gestion d'inscriptions

Ce dépôt contient le programme de gestion d'inscriptions utilisé pour l'édition 2023 du festival La Tenaille.

## Installation 

```
pip install git+https://codeberg.org/bib/tenaille23-inscriptions.git
```

## Utilisation

Executer `tenaille23-inscriptions --help` pour plus d'informations.
